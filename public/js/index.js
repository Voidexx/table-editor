'use strict'

main();

function onInput() {
    if (this.textContent === '') {
        this.textContent = '';
        horizontalSum();
        verticalSum();
        fetchData(tableToJSON());
        return;
    }

    if (!+this.textContent) {
        showWarning()
        let textContent = this.textContent.match(/\d/g);
        if (textContent !== null) {
            this.textContent = Array.prototype.reduce.call(textContent, (sum, current) => sum + current);
            horizontalSum();
            verticalSum();
            fetchData(tableToJSON());
            return;
        }

        this.textContent = '';
    }

    horizontalSum();
    verticalSum();
    fetchData(tableToJSON());
}

function horizontalSum() {
    const rows = Array.prototype.slice.call(document.querySelectorAll('.row'));
    let sum = 0;
    let index = 0;
    rows.forEach(row => {
        Array.prototype.forEach.call(row.children, cell => {
            if (cell === row.lastElementChild)
                return;

            sum += +cell.textContent;
            index++;
        });

        row.children[index].textContent = '';
        row.children[index].textContent = sum;
        sum = 0;
        index = 0;
    });
}

function verticalSum() {
    const rows = document.querySelectorAll('.row');
    const resultsRow = document.querySelectorAll('.verticalResults .cell');

    let sum = 0;
    Array.prototype.forEach.call(resultsRow, (cell, i) => {
        Array.prototype.forEach.call(rows, row => {
            sum += +row.children[i].textContent;
        });
        cell.textContent = '';
        cell.textContent = sum;
        sum = 0;
    });

    return;
}

function tableToJSON() {
    const table = document.querySelector('.table');
    let data = [];
    [].forEach.call(table.children, row => {
        let temp = { row: [] };
        [].forEach.call(row.children, cell => {
            temp.row.push(cell.textContent);
        });
        data.push(temp);
    });

    return JSON.stringify(data);
}

function showWarning() {
    const container = document.getElementById('warningContainer');
    if (container.children.length) {
        return;
    }
    const warning = document.createElement('h3');
    warning.textContent = 'Введите пожалуйста число!';
    container.appendChild(warning);
    setTimeout(() => warning.remove(), 1000);
    return;
}

function fetchData(data) {
    fetch('http://localhost:3000/data', {
        method: 'post', mode: 'cors', headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, body: data
    })
        .then(res => console.log(res.text()))
        .catch(error => console.log(error.message));
}


function saveTable() {
    fetch('http://localhost:3000/data', { method: 'get' })
        .then(res => res.blob())
        .then(blob => {
            let a = document.createElement('a');
            let url = URL.createObjectURL(blob);
            a.href = url;
            a.download = 'table.csv';
            a.click();
        })
        .catch(error => console.log(error.message));
}

function getDataFromCsv() {
    fetch('http://localhost:3000/getdata', { method: 'get' })
        .then(res => res.json())
        .then(data => render(data))
        .catch(error => console.log(error.message));
}


function render(data) {
    const rows = document.querySelector('.table').children;
    data.forEach((elem, i) => {
        elem.forEach((value, j) => {
            rows[i].children[j].textContent = value;
        });
    });
}

function main() {
    window.addEventListener('load', getDataFromCsv());

    document.querySelector('.table').addEventListener('input', function (event) {
        if (event.target.className !== 'cell')
            return;

        onInput.call(event.target);
    });

    document.querySelector('.saveButton').addEventListener('click', saveTable);
}