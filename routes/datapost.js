const fs = require('fs');

function dataFromClient(req, res) {
    let body = '';

    req.on('data', chunk => {
        body += chunk;
    });

    req.on('end', function () {
        body = JSON.parse(body);
        toCsvParse(body);
    });

    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Data is recived!');
    res.end();
}

function toCsvParse(data) {
    let arr = [];

    data.forEach((elem) => {
        let temp = elem.row.join(';');
        temp += '\n';
        arr.push(temp);
    });

    const stream = fs.createWriteStream('./data/table.csv', { flags: 'w', encoding: 'utf8', mode: 0o666 });
    arr.forEach(string => stream.write(string));
    return;
}

module.exports = dataFromClient;