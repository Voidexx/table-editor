const fs = require('fs');
const path = require('path');

function dataFromCsv(req, res) {
    let stream = fs.createReadStream(path.resolve('data', 'table.csv'));
    let buffer = '';
    stream.on('data', (chunk) => {
        buffer += chunk;
    });

    stream.on('end', () => {
        buffer = buffer.split('\n');
        buffer = buffer.map(elem => elem.split(';'));
        buffer.length = buffer.length - 1;
        buffer = JSON.stringify(buffer);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.write(buffer);
        res.end();
    });
}

module.exports = dataFromCsv;