const public = require('./public');
const home = require('./home');
const dataFromClient = require('./datapost');
const getData = require('./getdata')
const dataFromCsv = require('./datafromcsv');

module.exports = {
    public,
    home,
    dataFromClient,
    getData,
    dataFromCsv
}