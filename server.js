const http = require('http');
const fs = require('fs');
const path = require('path');
const { public, home, dataFromClient, getData, dataFromCsv } = require('./routes');


http.createServer((req, res) => {
    if (req.url.match(/\.(html|css|js|png|ico)$/)) {
        public(req, res);
    } else if (req.url === '/') {
        home(req, res);
    } else if (req.url === '/data' && req.method === 'POST') {
        dataFromClient(req, res);
    } else if (req.url === '/data' && req.method === 'GET') {
        getData(req, res);
    } else if (req.url === '/getdata' && req.method === 'GET') {
        dataFromCsv(req, res);
    }
}).listen(3000, () => console.log('Server ON!'));